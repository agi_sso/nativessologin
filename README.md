# NativeSSOLogin

[![CI Status](http://img.shields.io/travis/Pankaj Verma/NativeSSOLogin.svg?style=flat)](https://travis-ci.org/Pankaj Verma/NativeSSOLogin)
[![Version](https://img.shields.io/cocoapods/v/NativeSSOLogin.svg?style=flat)](http://cocoapods.org/pods/NativeSSOLogin)
[![License](https://img.shields.io/cocoapods/l/NativeSSOLogin.svg?style=flat)](http://cocoapods.org/pods/NativeSSOLogin)
[![Platform](https://img.shields.io/cocoapods/p/NativeSSOLogin.svg?style=flat)](http://cocoapods.org/pods/NativeSSOLogin)




## Table of Contents    
*     Introduction    
*     User Profile Handling      
*     Local and Global login sessions    
*     Prerequisites to use Cross app SSO     
*     iOS Installation   
*     List of APIs
*     FAQ    




## Introduction

Cross app Native SSO Integration is an authentication system that permits a user to login with email/ mobile and password/OTP on a single app and get auto login on across the Times network with cross app SSO integration.
SSO - Single Sign On centrally manages the authentication of users across the Times network. SSO handles both the sub domain and cross domain login.
It allows users to connect over cross domain sites without using their credentials.
For registration/login, at least one of email address or mobile is required. It is possible for a SSO user to have only email and no mobile, or vice versa or both. If the user has both email and mobile in his profile, he will be able to login using either of them as the identifier. 





## User Profile Handling

Basic user profile is taken care by SSO which includes   
**User Name** : first name and last name  
**Date of birth**  
**Email**: maximum 3 emails  
**Mobile**: maximum 1 mobile number  
**Social Account** : Fb, Gp  
**Gender** : Mr. and Mrs.  
**User Image**:  profile pic   
**Password**: User can change or update their password  







## Global and App Session

SSO will introduce global and local/App login session on the app in native version. Global session will be accessible to all the Apps of the (TIL) family while App session will be private for App. App session will identify if App is in login state, log out state, installed first time etc. Session stores following credentials:   
**tgId**: unique for a device.  
**ssec**: encrypted form of ssoid  
**ticketId**: ticket id of logged in user.  
**type**: type of login (facebook,Google plus or sso(email/mobile))  
**identifier**: will be user’s email/mobile number if login with email/mobile (i.i. sso)  

App session’s  ssec confirms if App is in login state. Empty type states no login type found i.e. App is doing login for the first time. 







## Pre-requisites to use Cross app SSO 

Business needs to provide the following properties which enables the app  to use Central created by SSO: -




Property | Description |Example| Mandatory
-- | -- | -- | --
Channel | Name of channel used by application. It is defined by business. | toicrossappnew | YES
Site Id | Name of site Id used by SSO BACKEND to authenticate the channel | bb51be5a5a8a0283467e0859d262ff6g | YES
Team Id | Apple Developer Team Id, can find in Apple developer member center. | <apple developer team id> | YES








## iOS Installation 

SDK can be included in project using cocoa pod. SDK is written in objective-c and can be implemented in both Objective-c and Swift projects. Facebook, Googleplus and Truecaller  pods are included externally to avoid any kind of transitive dependencies in Swift projects. 

Include following in your podfile.  
```ruby
pod 'NativeSSOLogin', :git =>'https://bitbucket.org/agi_sso/nativessologin.git'
```
(Warning: If you are coping above recheck the formats of symbols like ‘ and ”” )



## Project Setting 

SDK uses keychain to save user credentials. To share credentials among various apps of same family/devTeam a group(com.til.shared) is created. group+teamId identify the shared keychain. Credentials stored in shared keychain are kept in encrypted form and not accessible by any third party app. To enable keychain: 

Go to **Project setting** —> **Capabilities** —> **Keychain sharing: ON**  
keychain groups: **com.til.shared* 




## List of APIs

---
## SDK initialization  


```objective-c
-(void)ssoSetupForChannel:(NSString *)channel
siteId:(NSString *)siteId
teamId:(NSString *)teamId
isLive:(Boolean)isLive
completion:(completionBlock)completion;
```
Each App has its channel and corresponding site id. SSO functionalitis will not work without these two.
Team id(get it from member center) is used for keychain sharing. Global session will not work if teamId is incorrect. 
If your development team id and production team id are not same, carefully  change them accordingly.  

__Error Codes:__  
4000 : SDK_NOT_INITIALIZED  
4101 : Field is not string  
401 : INVALID_CHANNEL  

---


## Social login, link, de-link, pic upload
### Login using social info (deprecated)
```objective-c
-(void)loginUsingSocialInfo:(NSDictionary *)info
success:(voidBlock)success
failure:(errorBlock)failure
__attribute__((deprecated("new available method is, performSocialActivity:_ usingSocialInfo:_ success:_ failure:_")));
```
Login via Facebook, Google or Truecaller, FB/Google: Get oauthId and accessToken from FB/Google and pass in bellow method.
Truecaller: Get payload and signature from truecaller and pass in bellow method.

__Error Codes:__  
4000 : SDK_NOT_INITIALIZED  
413 : INVALID_REQUEST  
401 : INVALID_CHANNEL  
400 : TRANSACTION_ERROR  
4100 : DATA_NIL  
4104 : OAUTH_ID_NOT_FOUND  
4105 : OAUTH_SITE_ID_NOT_FOUND     
4106 : ACCESS_TOKEN_NOT_FOUND   

###  Link social (deprecated)

```objective-c 
-(void)linkSocialAccountUsingInfo:(NSDictionary *)info
success:(voidBlock)success
failure:(errorBlock)failure
__attribute__((deprecated("new available method is, performSocialActivity:_ usingSocialInfo:_ success:_ failure:_")));
```
Get oauthId and accessToken fron FB/GP and pass in bellow function to link with SSO account.  

__Error Codes:__  
4000 : SDK_NOT_INITIALIZED  
4100 : DATA_NIL  
413 : INVALID_REQUEST  
400 : TRANSACTION_ERROR  
4104 : OAUTH_ID_NOT_FOUND  
4105 : OAUTH_SITE_ID_NOT_FOUND  
4106 : ACCESS_TOKEN_NOT_FOUND 


### Delink facebook (deprecated) 
```objective-c 
-(void)delinkFacebook:(voidBlock)success
failure:(errorBlock)failure
__attribute__((deprecated("new available method is, performSocialActivity:_ usingSocialInfo:_ success:_ failure:_")));
```
Facebook account will be de-linked from SSO.  

__Error Codes:__  
4000 : SDK_NOT_INITIALIZED  
413 : INVALID_REQUEST  
400 : TRANSACTION_ERROR  


### Delink googleplus (deprecated)
```objective-c 
-(void)delinkGoogleplus:(voidBlock)success
failure:(errorBlock)failure
__attribute__((deprecated("new available method is, performSocialActivity:_ usingSocialInfo:_ success:_ failure:_")));
```
Google account will be de-linked from SSO.  

__Error Codes:__  
4000 : SDK_NOT_INITIALIZED  
413 : INVALID_REQUEST  
400 : TRANSACTION_ERROR   

### Social image upload (deprecated)

```objective-c 
-(void)uploadProfilePicFromSocialUsingInfo:(NSDictionary *)info
success:(voidBlock)success
failure:(errorBlock)failure
__attribute__((deprecated("new available method is, performSocialActivity:_ usingSocialInfo:_ success:_ failure:_")));
```
Get oauthId and accessToken fron FB/GP and pass in bellow function. This will make your social profile pic as SSO profile pic.  

__Error Codes:__ 

### Perform social activity  (new)
```objective-c 
-(void)performSocialActivity:(SSOSocialActivityOptions)option
usingSocialInfo:(SSOSocialInfo * _Nullable)info
success:(voidBlock)success
failure:(errorBlock)failure;
```
Login via Facebook,Google or Truecaller; link/d-link Facebook or Google account to SSO; upload profile pic from Gggole or Facebook account
__Note__: Truecaller login is in beta phase.  
For facebook and Google set oauthId and accessToken.  
For Trucaller(only login supported) set payload and signature.  


__Error Codes:__  
4000 : SDK_NOT_INITIALIZED  
413 : INVALID_REQUEST  
401 : INVALID_CHANNEL  
400 : TRANSACTION_ERROR  
4100 : DATA_NIL  
4104 : OAUTH_ID_NOT_FOUND  
4105 : OAUTH_SITE_ID_NOT_FOUND  
4106 : ACCESS_TOKEN_NOT_FOUND  



---
## Login via email or mobile

### Send login otp
```objective-c
-(void)sendLoginOtpOnEmail:(NSString *)email
mobile:(NSString *)mobile
success:(voidBlock)success
failure:(errorBlock)failure;
```
User can login via their registered email or mobile.
If user know their password they can login with registered email/mobile (by calling verifyLoginOtpPassword:) or
they can request a login OTP on one of their registered email/mobile.

__Error Codes:__  
4000 : SDK_NOT_INITIALIZED  
413 : INVALID_REQUEST  
401 : INVALID_CHANNEL  
400 : TRANSACTION_ERROR  
427 : PROXY_OR_DEFUNC_EMAIL  
428 : INDIATIMES_EMAIL  
405 : UNVERIFIED_EMAIL  
407 : UNREGISTERED_EMAIL  
403 : INVALID_EMAIL  
4101 : Field is not string  

### Login with otp or password
```objective-c
-(void)verifyLoginOtpPassword:(NSString *)password
email:(NSString *)email
mobile:(NSString *)mobile
success:(voidBlock)success
failure:(errorBlock)failure;
```   
After verification user will be loged in.  

__Error Codes:__  
4000 : SDK_NOT_INITIALIZED  
413 : INVALID_REQUEST  
402 : INVALID_MOBILE  
401 : INVALID_CHANNEL  
408 : UNREGISTERED_MOBILE  
406 : UNVERIFIED_MOBILE  
400 : TRANSACTION_ERROR  
427 : PROXY_OR_DEFUNC_EMAIL  
4101 : Field is not string   

---

## New user registration or signup

### Signup (deprecated)
```objective-c
-(void)registerUser:(NSString *)name
mobile:(NSString *)mobile
email:(NSString *)email
password:(NSString *)password
gender:(NSString *)gender
isSendOfferEnabled:(BOOL)isSendOfferEnabled
success:(voidBlock)success
failure:(errorBlock)failure
__attribute__((deprecated("new available method is, performSignupActivity :_ forUser:_ success:_ failure:_")));
```
User can provide either mobile or email or both.
User will provede their gender, choose a password(following standerd) and agree to send offer notifications(YES or NO).
All fields are mandatory except one of email or mobile can be blank.
A sign up otp is preferably send on mobile.
If mobile is not provided otp will be send on email.
User can resend OTP.
User will be logged in once OTP is successfully verified.  

__Error Codes:__   
4000 : SDK_NOT_INITIALIZED  
420 : INVALID_NAME  
421 : INVALID_GENDER  
413 : INVALID_REQUEST  
403 : INVALID_EMAIL  
402 : INVALID_MOBILE  
417 : INVALID_PASSWORD  
429: ALREADY_REGISTERED_USER  
400 : TRANSACTION_ERROR  
401 : INVALID_CHANNEL   


### Signup (new)
```objective-c 
-(void)performSignupActivity:(SSOSignupOptions)options
forUser:(SSOSignupUser *)user
success:(voidBlock)success
failure:(errorBlock)failure;
```
User can signup by providing their full details or just mobile number and name.

__Error Codes:__  
4000 : SDK_NOT_INITIALIZED  
420 : INVALID_NAME  
421 : INVALID_GENDER  
413 : INVALID_REQUEST  
403 : INVALID_EMAIL  
402 : INVALID_MOBILE  
417 : INVALID_PASSWORD  
429: ALREADY_REGISTERED_USER  
400 : TRANSACTION_ERROR  
401 : INVALID_CHANNEL  
4101 : Field is not string    

### Resend signup otp
```objective-c
-(void)resendSignUpOtpForEmail:(NSString *)email
mobile:(NSString *)mobile
success:(voidBlock)success
failure:(errorBlock)failure;
```
User can resend signup otp. Otp is valid up to 10 minutes.  

__Error Codes:__  
4000 : SDK_NOT_INITIALIZED  
413 : INVALID_REQUEST  
403 : INVALID_EMAIL  
402 : INVALID_MOBILE  
429 : ALREADY_REGISTERED_USER  
400 : TRANSACTION_ERROR  
407 : UNREGISTERED_EMAIL  
401 : INVALID_CHANNEL  
4101 : Field is not string   

### Verify signup otp
```objective-c
-(void)verfiySignUpOTP:(NSString *)otp
email:(NSString *)email
mobile:(NSString *)mobile
success:(voidBlock)success
failure:(errorBlock)failure;
```
After verification user will be registered and logedin as well.  

__Error Codes:__  
4000 : SDK_NOT_INITIALIZED  
413 : INVALID_REQUEST  
424 : INVALID_OTP  
403 : INVALID_EMAIL  
402 : INVALID_MOBILE  
429 : ALREADY_REGISTERED_USER  
416 : LIMIT_EXCEEDED  
414 : WRONG_OTP  
400 : TRANSACTION_ERROR  
401 : INVALID_CHANNEL  
415 : EXPIRED_OTP  
4101 : Field is not string   



---
## Signout user

```objective-c
-(void)signOutUser:(voidBlock)success
failure:(errorBlock)failure;
```
User will be loged out.
Logging out will delete App session. Login type and identifier will not be deleted from App session.
Global session will be deleted only if it is same as App session.  

__Error Codes:__  
4000 : SDK_NOT_INITIALIZED  
401 : INVALID_CHANNEL  
413 : INVALID_REQUEST  
400 : TRANSACTION_ERROR  


---
## Password

### Change password
```objective-c
-(void)changePassword:(NSString *)oldPassword
newPassword:(NSString *)newPassword
confirmPassword:(NSString *)confirmPassword
success:(voidBlock)success
failure:(errorBlock)failure;
```
User can change their password.
New password must be different from previous three passwords.  

__Error Codes:__   
4000 : SDK_NOT_INITIALIZED  
401 : INVALID_CHANNEL  
404 : UNAUTHORIZED_ACCESS  
413 : INVALID_REQUEST  
417 : INVALID_PASSWORD  
418 : PASSWORD_MATCHES_LAST_THREE  
419 : WRONG_PASSWORD  
400 : TRANSACTION_ERROR  
4101 : Field is not string    


### Validate password  
```objective-c 
-(void)validatePassword:(NSString *)password
confirmPassword:(NSString *)confirmPassword
email:(NSString *)email
mobile:(NSString *)mobile
success:(voidBlock)success
failure:(errorBlock)failure;
```
This API will help user for password validation like allowed characters, password should not match last 3 passords etc.    

__Error Codes:__  


### Forgot password otp

```objective-c 
-(void)getForgotPasswordOTPForEmail:(NSString *)email
mobile:(NSString *)mobile
success:(voidBlock)success
failure:(errorBlock)failure ;
```
If user can request for forgot password OTP on one of their registered email/mobile and with this OTP he/she can provide/create new password.
New password must not match previous three passwords.   

__Error Codes:__   
4000 : SDK_NOT_INITIALIZED  
408 : UNREGISTERED_MOBILE  
406 : UNVERIFIED_MOBILE  
402 : INVALID_MOBILE  
410 : INVALID_IDENTIFIER  
411 : TOKEN_GENERATION_ERROR  
412 : SMS_FAILURE  
401 : INVALID_CHANNEL  
4101 : Field is not string     

###  Resend forgot passord otp
```objective-c 
-(void)resendForgotPasswordOTPForEmail:(NSString *)email
mobile:(NSString *)mobile
success:(voidBlock)success
failure:(errorBlock)failure;
```
User can resend otp. Otp will be valid for 10 minutes.    

__Error Codes:__    
4000 : SDK_NOT_INITIALIZED  
4101 : Field is not string  
413 : INVALID_REQUEST  
407 : UNREGISTERED_MOBILE  
406 : UNVERIFIED_MOBILE  
402 : INVALID_MOBILE  
401 : INVALID_CHANNEL  
410 : INVALID_IDENTIFIER   

### Verify forgot password otp
```objective-c 
-(void)verifyForgotPasswordForEmail:(NSString *)email
mobile:(NSString *)mobile
otp:(NSString *)otp
password:(NSString *)password
confirmPassword:(NSString *)confirmPassword
success:(voidBlock)success
failure:(errorBlock)failure;
```
Password will be changed after successful verification.   

__Error Codes:__   
4000 : SDK_NOT_INITIALIZED  
4101 : Field is not string   
413 : INVALID_REQUEST  
417 : INVALID_PASSWORD  
418 : PASSWORD_MATCHES_LAST_THREE  
419 : WRONG_PASSWORD  
400 : TRANSACTION_ERROR  


---
## Add email or mobile  
A SSO user can have maximum 3 emails and 1 mobile.  

### Add email
```objective-c 
-(void)addAlternateEmail:(NSString *)email
success:(voidBlock)success
failure:(errorBlock)failure;
```
opt will be sent on provided email  

__Error Codes:__  
4000:      SDK_NOT_INITIALIZED  
4001:      NO_INTERNET_CONNECTION  
4002:     REQUEST_FAILED  
4003:      NETWORK_ERROR             
413:        INVALID_REQUEST   
404:        UNAUTHORIZED_ACCESS  
401:        INVALID_CHANNEL  
403:        INVALID_EMAIL  
435:        ADD_EMAIL_MAX_LIMIT_EXCEEDED  
433:        ALREADY_VERIFIED  
400:       TRANSACTION_ERROR  

### Verify email
```objective-c 
-(void)verifyAddAlternateEmailOtp:(NSString *)otp
forEmail:(NSString *)email
success:(voidBlock)success
failure:(errorBlock)failure ;
```
After verification email will be added and can be get in userDetails.   

__Error Codes:__  
4000:    SDK_NOT_INITIALIZED  
4001:    NO_INTERNET_CONNECTION  
4002:     REQUEST_FAILED  
4003:    NETWORK_ERROR                
413:      INVALID_REQUEST  
404:      UNAUTHORIZED_ACCESS  
401:      INVALID_CHANNEL  
403:      INVALID_EMAIL  
435:      ADD_EMAIL_MAX_LIMIT_EXCEEDED  
433:      ALREADY_VERIFIED  
400:     TRANSACTION_ERROR  
415:      EXPIRED_OTP  
416:      LIMIT_EXCEEDED  
414:      WRONG_OTP   

### Add(update) mobile
```objective-c 
-(void)updateMobile: (NSString *) mobile
success:(voidBlock)success
failure:(errorBlock)failure;
``` 
otp will be sent on provided mobile.  

__Error Codes:__  
4000:    SDK_NOT_INITIALIZED  
4001:    NO_INTERNET_CONNECTION  
4002:     REQUEST_FAILED  
4003:     NETWORK_ERROR  
413:      INVALID_REQUEST  
404:      UNAUTHORIZED_ACCESS  
401:      INVALID_CHANNEL  
402:      INVALID_MOBILE  
432:      BLOCKED_MOBILE  
433:      ALREADY_VERIFIED  
400:      TRANSACTION_ERROR   


### Verify mobile
```objective-c 
-(void)verifyUpdateMobileOtp:(NSString *)otp
forMobile:(NSString *)mobile
success:(voidBlock)success
failure:(errorBlock)failure;
```
After successful update old mobile will be removed from user.   

__Error Codes:__  
4000:     SDK_NOT_INITIALIZED  
4001:     NO_INTERNET_CONNECTION  
4002:     REQUEST_FAILED  
4003:      NETWORK_ERROR  
413:        INVALID_REQUEST  
404:        UNAUTHORIZED_ACCESS  
401:        INVALID_CHANNEL  
402:        INVALID_MOBILE  
432:        BLOCKED_MOBILE  
433:        ALREADY_VERIFIED  
400:        TRANSACTION_ERROR  
434:         NOT_FOUND  
415:        EXPIRED_OTP  
416:        LIMIT_EXCEEDED  
414:       WRONG_OTP  



---
## Login sessions


### Global session (deprecated)

```objective-c 
-(void)getGlobalSessionOnCompletion:(completionBlock)completion
__attribute__((deprecated("new available method is,getGlobalSessionWithUserDataEnabled:_ completion:_")));
```
Globle session will be availble if SSO user has already loged in in some other App of TIL in the device. To make use of global session copy it to you app session.
__Error Codes:__  
4000 : SDK_NOT_INITIALIZED  
4103 : SESSION_NOT_FOUND  

### App session (deprecated)
```objective-c 
-(void)getAppSessionOnCompletion:(completionBlock)completion
__attribute__((deprecated("new available method is,getSSOAppSessionOnCompletion:_")));
```
__Error Codes:__  
4000 : SDK_NOT_INITIALIZED  



### Global session (new)
```objective-c 
-(void)getSSOGlobalSessionWithUserDataEnabled:(Boolean)userDataEnabled
completion:(sessionBlock)completion;
```
Globle session will be availble if SSO user has already loged in in some other App of TIL in the device. To make use of global session copy it to you app session.
If userDataEnabled is true, it will return session + basic info of global user else only global session will be returnd. Result will be an object of type SSOSession.  

__Error Codes:__  
4000 : SDK_NOT_INITIALIZED  
4101 : Field is not string  

### App session (new)
```objective-c 
-(void)getSSOAppSessionOnCompletion:(sessionBlock)completion;
```
result will be an object of type SSOSession  

__Error Codes:__   
4000 : SDK_NOT_INITIALIZED  

### Copy session

```objective-c 
-(void)copySSOGlobalSessionToAppOnCompletion:(completionBlock)completion
;
```
This is Cross app login and works silentely.
If User is new and Global session exist, he/she can contine with global session.
Global session will be copied to App with new refreshed ticketId.
Note: Existing user who are in logout state before integration of This SDK in the App will be treated as new user and will be (may be )silentely login after this update. They are requested to logout if not happy or want to login with different account.  

__Error Codes:__ 

### Creating session from ticket
```objective-c 
-(void) createAppSessionForTicketId:(NSString *)ticketId completion:(completionBlock)completion;
```
This will create app session for unverified users. Session will be saved only in App session not in Global session.  

__Error Codes:__   
4000 : SDK_NOT_INITIALIZED  
4101 : Field is not string   

### Session migration

```objective-c 
-(void)migrateCurrentSessionToAppHavingTicketId:(NSString *)ticketId
completion:(completionBlock)completion;
```
Apps which are using their own login have to migrate their login session to SSO session so that users which are already login in App will also be in login state after update(integration of NativeSSO SDK).
This is one time call.
After migration done App can remove its own session.   

__Error Codes:__  
4000 : SDK_NOT_INITIALIZED  
4101 : Field is not string

---
## Renew ticket
```objective-c 
-(void )renewTicket:(void(^)(void))success
failure:(errorBlock)failure;
```
This API will reset the life time of ticket to 30 days from now of the log in user .   

__Error Codes:__  
4000:      SDK_NOT_INITIALIZED  
4001:      NO_INTERNET_CONNECTION  
4003:      NETWORK_ERROR            
413:        INVALID_REQUEST   
404:        UNAUTHORIZED_ACCESS  
401:        INVALID_CHANNEL  

---
## User details
### User details (deprecated)
```objective-c
-(void)getUserDetails:(void(^)(NSDictionary *info))success
failure:(void(^)(NSError * _Nullable error))failure
__attribute__((deprecated("new available method is, getUserDetailsOnCompletion")));
```  
User details ccontains date of birth(dob), profile pic url(dp), user's registered email list(emailList),First Name, Last Name, Gender, user's registered email list(mobileList), primary email, ssoId and whether Facebbok or Google plus account is connected with SSO or not.  

__Error Codes:__  
4000 : SDK_NOT_INITIALIZED   
401 : INVALID_CHANNEL  
404 : UNAUTHORIZED_ACCESS  


### User details (new)

```objective-c 
-(void)getUserDetailsOnCompletion:(userDetailsBlock)completion;
```
user details will be returned in an object of type SSOUserDetails.  

__Error Codes:__ 
4000 : SDK_NOT_INITIALIZED
401 : INVALID_CHANNEL
404 : UNAUTHORIZED_ACCESS

### User details from local
```objective-c 
-(SSOUserDetails *)getUserDetailsLocal;
```
This will return user details stored in device. No network call.  

__Error Codes:__  
NA

### Update user details (deprecated)

```objective-c 
-(void)updateFirstName:(NSString *)firstName
lastName:(NSString *)lastName
dob:(NSString *)dob
gender:(NSString *)gender
success:(voidBlock)success
failure:(errorBlock)failure
__attribute__((deprecated("new available method is, updateUserDetails:_ success:_ failure:_")));
```
User can update their name, dob and gender. The field which you do not want to update leave them blank(empty string). On success updated details will be returned.  

### Update user details (new)

```objective-c 
-(void)updateUserDetails:(SSOUserUpdates *)userDetails
success:(userUpdatesBlock)success
failure:(errorBlock)failure;
```
update user details : first name, last name, gender(M or F), dob,city. Leave the field blank which you do not want to update.
Updated details will be returned in an object of type SSOUserUpdates   

__Error Codes:__   
4000 : SDK_NOT_INITIALIZED   
4101 : Field is not string   
413 : INVALID_REQUEST  
400 : TRANSACTION_ERROR  

---

## Upload profile pic
###  Upload pic (deprecated)
```objective-c 
-(void)openPhotoSelectorOnViewController:(UIViewController *)vc
success:(voidBlock)success
failure:(errorBlock)failure
__attribute__((deprecated("new available method is, performPickUploadActivity:_ onController:_ startRequest:_ success:_ failure:_")));
```
upload pic from camera or gallery  

__Error Codes:__  
4000 : SDK_NOT_INITIALIZED  
4100 : DATA_NIL  
413 : INVALID_REQUEST  
400 : TRANSACTION_ERROR  
4104 : OAUTH_ID_NOT_FOUND  
4105 : OAUTH_SITE_ID_NOT_FOUND  
4106 : ACCESS_TOKEN_NOT_FOUND  

### Upload pic (new)
```objective-c 
-(void)performPickUploadActivity:(SSOPickUploadOptions)options
onController:(UIViewController *)vc
startRequest:(imageUploadStart) uploadStart
success:(successBlock)success
failure:(errorBlock)failure;
```
upload from camera/gallery  

__Error Codes:__   
4000 : SDK_NOT_INITIALIZED  
413 : INVALID_REQUEST  
400 : TRANSACTION_ERROR  

---

## Status for identifier
```objective-c 
-(void)getStatusForIdentifier:(NSString *)identifier
success:(userStatusBlock)success
failure:(errorBlock)failure;
```
This Api will check if user(email/mobile) is registered with SSO). Status will be returned in an object of type SSOUserStatus.  

__Error Codes:__   
4000 : SDK_NOT_INITIALIZED  
4101 : Field is not string   
413 : INVALID_REQUEST  
400 : TRANSACTION_ERROR  



---

## Login with gdpr details

```objective-c 
-(void)loginWithGdprDetails:(GdprDetails *)gdprDetails
success:(voidBlock)success
failure:(errorBlock)failure;
```
It works with almost all markdown flavours (the below blank line matters).   


__Errors codes:__  
4000 : SDK_NOT_INITIALIZED  
413  : INVALID_REQUEST  
401  : INVALID_CHANNEL  
400  : TRANSACTION_ERROR  
427  : PROXY_OR_DEFUNC_EMAIL   

---
## BlockUser

```objective-c
-(void)blockUser:(voidBlock)success
failure:(errorBlock)failure;
```
User will be blocked for the business(channel).    

__Error Codes:__    


//-(void)deleteUserAccountWithPassword:(NSString *)password onCompletion:(completionBlock)completion;

---


NSSOCrossAppLoginManager is a Singleton class. Only one instance will be created no matter whether you are instantiating it using –()init method or +()sharedLoginManager class method. 

NSSOCrossAppLoginManager *manager = [NSSOCrossAppLoginManager sharedLoginManager]; 



**Notes:**

Global session will not be created for invalid teamId i.e. Cross App Login will not work but other functionalities will work perfectly. 
If your deployment target is 7, include Security.framework (with status: required) and CoreFoundation.framework(with status: optional)
At the time of production, developer team id must be replaced with Production team id in appDelegate.




## FAQ

__Q:__ What is the minimum version supported by SSO for android and IOS  
__A:__ for Android version 4.1 and for IOS version 7  

__Q:__  How this new version of SSO is useful for all the business?  
__A:__ It is useful as user can manage his account via single app and it will be reflected in the other apps. User will be auto login at the time of installation of app so app can have more logged in users.  

__Q:__ Will my user data is consistent across the network?  
__A:__ Yes, User’s basic profile will remain same over the network as it is stored in SSO only,   

__Q:__  Does SSO support user session across app?  
__A:__ Yes, SSO supports cross app login.  

__Q:__  Can I use non Gmail account from cross app login?  
__A:__  Yes. User can logout and login with desired email.  



Thank you!



## Author

Pankaj Verma, pankaj.verma@timesinternet.in  

## License

NativeSSOLogin is available under the MIT license. See the LICENSE file for more info.  

