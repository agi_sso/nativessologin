//
//  NativeSSOLogin_ExampleUITests.m
//  NativeSSOLogin_ExampleUITests
//
//  Created by Pankaj Verma on 27/03/17.
//  Copyright © 2017 Pankaj Verma. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface NativeSSOLogin_ExampleUITests : XCTestCase

@end

@implementation NativeSSOLogin_ExampleUITests

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    [[[XCUIApplication alloc] init] launch];
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [app.buttons[@"LOGIN"] tap];
    [app.buttons[@"SEND OTP EMAIL"] tap];
    XCUIElement *okButton = app.alerts[@"Alert!!"].buttons[@"OK"];
    [okButton tap];
    [app.buttons[@"SEND OTP MOBILE"] tap];
    [okButton tap];
    [app.buttons[@"RESEND OTP EMAIL"] tap];
    [okButton tap];
    [app.buttons[@"RESEND OTP MOBILE"] tap];
    [okButton tap];
    [app.buttons[@"SIGN IN WITH EMAIL"] tap];
    [okButton tap];
    [app.buttons[@"SIGN IN WITH MOBILE"] tap];
    [okButton tap];
    [app.buttons[@"ADD ALTERNATE EMAIL"] tap];
    [okButton tap];
    [app.buttons[@"UPDATE MOBILE"] tap];
    [okButton tap];
    [app.buttons[@"VERIFY ADD ALETRNATE EMAIL OTP"] tap];
    [okButton tap];
    [app.buttons[@"VERIFY UPDATE MOBILE OTP"] tap];
    [okButton tap];
}


-(void)testForEmptyInputFields
{
    XCUIApplication *app = [[XCUIApplication alloc] init];
        XCUIElement *okButton = app.alerts[@"Alert!!"].buttons[@"OK"];
    [app.buttons[@"LOGIN"] tap];
    [app.buttons[@"SEND OTP EMAIL"] tap];
    [okButton tap];
    [app.buttons[@"SEND OTP MOBILE"] tap];
    [okButton tap];
    [app.buttons[@"RESEND OTP EMAIL"] tap];
    [okButton tap];
    [app.buttons[@"RESEND OTP MOBILE"] tap];
    [okButton tap];
    [app.buttons[@"SIGN IN WITH EMAIL"] tap];
    [okButton tap];
    [app.buttons[@"SIGN IN WITH MOBILE"] tap];
    [okButton tap];
    [app.buttons[@"ADD ALTERNATE EMAIL"] tap];
    [okButton tap];
    [app.buttons[@"VERIFY ADD ALETRNATE EMAIL OTP"] tap];
    [okButton tap];
    [app.buttons[@"UPDATE MOBILE"] tap];
    [okButton tap];
    [app.buttons[@"VERIFY UPDATE MOBILE OTP"] tap];
    [okButton tap];
    
    [app.buttons[@"Close"] tap];
    
    
    [app.buttons[@"SIGNUP"] tap];
    [app.buttons[@"Signup"] tap];
    
    [okButton tap];
    [app.buttons[@"Verify OTP: Mobile"] tap];
    [okButton tap];
    [app.buttons[@"Resend OTP : Mobile"] tap];
    [okButton tap];
    [app.buttons[@"Verify OTP : Email"] tap];
    [okButton tap];
    [app.buttons[@"Resend OTP: Email"] tap];
    [okButton tap];
    [app.buttons[@"Close"] tap];
    
    
    [app.buttons[@"CHANGE PASSWORD"] tap];
    [app.buttons[@"CHANGE PASSWORD"] tap];
    [okButton tap];
    [app.buttons[@"Close"] tap];
    
    
    [app.buttons[@"FORGOT PASSWORD"] tap];
    [app.buttons[@"SEND FP OTP MOBILE "] tap];
    
    [okButton tap];
    [app.buttons[@"SEND FP OTP EMAIL"] tap];
    [okButton tap];
    [app.buttons[@"VERIFY OTP MOBILE"] tap];
    [okButton tap];
    [app.buttons[@"RESEND OTP MOBILE"] tap];
    [okButton tap];
    [app.buttons[@"VERIFY OTP EMAIL"] tap];
    [okButton tap];
    [app.buttons[@"RESEND OTP EMAIL"] tap];
    [okButton tap];
    [app.buttons[@"CHECK USER STATUS"] tap];
    [okButton tap];
    [app.buttons[@"Close"] tap];
    
    
    
    [app.buttons[@"Update"] tap];
    [app.buttons[@"Update"] tap];
    [okButton tap];
    [app.buttons[@"Close"] tap];
    
    
   
    [app.buttons[@"Social"] tap];
    [app.buttons[@"Upload Profile Pic"] tap];
    [app.alerts[@"UPLOAD PROFILE PICTURE"].buttons[@"Photo Gallery    "] tap];
    [app.collectionViews[@"PhotosGridView"].cells[@"Photo, Landscape, March 13, 2011, 5:47 AM"] tap];
    [[[[[app childrenMatchingType:XCUIElementTypeWindow] elementBoundByIndex:0] childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:1] tap];
    [app.alerts[@"Error"].buttons[@"OK"] tap];
    [app.buttons[@"Close"] tap];
    
}
-(void)testEmailLoginWithPassword
{
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [app.buttons[@"LOGIN"] tap];
    XCUIElement *emtf = app.textFields[@"Enter Mobile or Email"];
    [emtf tap];
    [emtf typeText:@"pankajverma232@gmail.com"];
    XCUIElement *optf = app.secureTextFields[@"Enter Password or Otp"];
    [optf tap];
    [optf typeText:@"toi@1234"];

    [app.buttons[@"SIGN IN WITH EMAIL"] tap];
    [app.alerts[@"Alert!!"].buttons[@"OK"] tap];
    
        [app.buttons[@"Close"] tap];
        [app.buttons[@"SIGNOUT"] tap];
        [app.alerts[@"Alert!!"].buttons[@"OK"] tap];

}
-(void)testMobileLoginWithPassword
{
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [app.buttons[@"LOGIN"] tap];
    XCUIElement *emtf = app.textFields[@"Enter Mobile or Email"];
    [emtf tap];
    [emtf typeText:@"pankajverma232@gmail.com"];
    XCUIElement *optf = app.secureTextFields[@"Enter Password or Otp"];
    [optf tap];
    [optf typeText:@"toi@1234"];
    
    [app.buttons[@"SIGN IN WITH EMAIL"] tap];
    [app.alerts[@"Alert!!"].buttons[@"OK"] tap];
    
    [app.buttons[@"Close"] tap];
    [app.buttons[@"SIGNOUT"] tap];
    [app.alerts[@"Alert!!"].buttons[@"OK"] tap];
    
}
-(void)testScreens
{
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [app.buttons[@"LOGIN"] tap];
    
    XCUIElement *closeButton = app.buttons[@"Close"];
    [closeButton tap];
    [app.buttons[@"SIGNUP"] tap];
    [closeButton tap];
    [app.buttons[@"CHANGE PASSWORD"] tap];
    [closeButton tap];
    [app.buttons[@"FORGOT PASSWORD"] tap];
    [closeButton tap];
    [app.buttons[@"Social"] tap];
    [closeButton tap];
    [app.buttons[@"Update"] tap];
    [closeButton tap];
    
}
@end
