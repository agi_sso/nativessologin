//
//  NSSORecoverViewController.m
//  NativeSSOLogin
//
//  Created by Pankaj Verma on 25/10/16.
//  Copyright © 2016 Pankaj Verma. All rights reserved.
//

#import "NSSORecoverPasswordViewController.h"
#import "NSSOCrossAppLoginManager.h"

@interface NSSORecoverPasswordViewController ()
@property (weak, nonatomic) IBOutlet UITextField *emailOrMobile;
@property (weak, nonatomic) IBOutlet UITextField *otp;
@property (weak, nonatomic) IBOutlet UITextField *confirmPassword;
@property (weak, nonatomic) IBOutlet UITextField *thePasswd;

@end
@implementation NSSORecoverPasswordViewController
NSSOCrossAppLoginManager *cavc ;
- (void)viewDidLoad {
    [super viewDidLoad];
    cavc = [[NSSOCrossAppLoginManager alloc]  init];
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapRecognizer];
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (IBAction)sendFPOTPOnMobile:(id)sender
{
    [cavc getForgotPasswordOTPForEmail:@"" mobile:_emailOrMobile.text success:^{
        [self showAlertMessage:[NSString stringWithFormat: @"Otp sent on Mobile: %@",_emailOrMobile.text]];
    } failure:^(NSError * error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];

}

- (IBAction)sendFPOTPOnEmail:(id)sender
{
    [cavc getForgotPasswordOTPForEmail:_emailOrMobile.text mobile:@"" success:^{
    [self showAlertMessage:[NSString stringWithFormat: @"Otp sent on : %@",_emailOrMobile.text]];
    } failure:^(NSError * error) {
       [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
}

- (IBAction)verifyFPOTPForMobile:(id)sender {
    [cavc verifyForgotPasswordForEmail:@"" mobile:_emailOrMobile.text otp:_otp.text password:_thePasswd.text confirmPassword:_confirmPassword.text success:^{
        [self showAlertMessage:@"Password changed"];
    } failure:^(NSError * error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
        
    
}
- (IBAction)verifyFPOTPForEmail:(id)sender
{
    [cavc verifyForgotPasswordForEmail:_emailOrMobile.text mobile:@"" otp:_otp.text password:_thePasswd.text confirmPassword:_confirmPassword.text success:^{
        [self showAlertMessage:@"Password changed"];
    } failure:^(NSError * error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
   
}

- (IBAction)resendFPOTPOnEmail:(id)sender
{
    [cavc resendForgotPasswordOTPForEmail:_emailOrMobile.text mobile:@"" success:^{
        [self showAlertMessage:[NSString stringWithFormat: @"Otp sent on : %@",_emailOrMobile.text]];
    } failure:^(NSError * error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
    
}

- (IBAction)resendFPOTPOnMobile:(id)sender {
    [cavc resendForgotPasswordOTPForEmail:@"" mobile:_emailOrMobile.text success:^{
        [self showAlertMessage:@"Success"];
    } failure:^(NSError * error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
    
}

- (IBAction)validatePasswordOnMobile:(id)sender {
    [[NSSOCrossAppLoginManager sharedLoginManager] validatePassword:_thePasswd.text confirmPassword:_confirmPassword.text email:@"" mobile:_emailOrMobile.text success:^{
        [self showAlertMessage:@"Password is Valid"];
    } failure:^(SSOError * _Nullable error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
        
    }];
}

- (IBAction)validatePasswordOnEmail:(id)sender {
    [[NSSOCrossAppLoginManager sharedLoginManager] validatePassword:_thePasswd.text confirmPassword:_confirmPassword.text email:_emailOrMobile.text mobile:@"" success:^{
        [self showAlertMessage:@"Password is Valid"];
    } failure:^(SSOError * _Nullable error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
        
    }];
}

- (IBAction)checkStatusForUser:(id)sender {

    [[NSSOCrossAppLoginManager sharedLoginManager] getStatusForIdentifier:_emailOrMobile.text
                                                                  success:^(SSOUserStatus * _Nullable user)
     {
         [self showAlertMessage:[self getUserString:user]];
         
     } failure:^(NSError * _Nullable error)
     {
         [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
     }];
}

-(NSString *)getUserString:(SSOUserStatus *)user
{
    return  [NSString stringWithFormat:@"identifier:%@\nstatus:%@\nstatusCode:%@\n",user.identifier,user.status,user.statusCode];
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

-(void)showAlertMessage:(NSString *)message{
    [[[UIAlertView alloc] initWithTitle:@"Alert!!" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}

@end
