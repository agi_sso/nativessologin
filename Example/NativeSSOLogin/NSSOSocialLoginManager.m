//
//  NSSOSocialLoginManager.m
//  Pods
//
//  Created by Pankaj Verma on 09/11/16.
//
//

#import "NSSOSocialLoginManager.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Google/SignIn.h>
#import <NativeSSOLogin/NSSOCrossAppLoginManager.h>
#import "NSSOTempGlobal.h"
#import <TrueSDK/TrueSDK.h>

#define SOCIAL_OAUTHID_KEY  @"oauthId"
#define SOCIAL_ACCESS_TOKEN_KEY  @"accessToken"
#define USER_PUBLIC_PROFILE_KEY  @"public_profile"
#define USER_EMAIL_KEY  @"email"
#define ERROR_DOMAIN @"agi_sso"
#define FB_LOGIN_FLOW_CANCELLED_CODE 4007
#define FB_LOGIN_FLOW_CANCELLED_MESSAGE @"FB_LOGIN_FLOW_CANCELLED"
#define TRUE_PAYLOAD_KEY @"payload"
#define TRUE_SIGNATURE_KEY @"signature"
#define TRUE_PROFILE_NIL_CODE 4007
#define TRUE_PROFILE_NIL_MESSAGE @"TRUE_PROFILE_NIL"
@interface NSSOSocialLoginManager ()<GIDSignInDelegate,GIDSignInUIDelegate,TCTrueSDKDelegate>
@end

@implementation NSSOSocialLoginManager

@synthesize privateGoogleLoginDelegate;
@synthesize privateTruecallerLoginDelegate;
static NSSOSocialLoginManager *singletonObject = nil;

+ (id) sharedManager
{
    if (! singletonObject)
    {
        singletonObject = [[NSSOSocialLoginManager alloc] init];
        [FBSDKAppEvents activateApp];
    }
    return singletonObject;
}

- (id)init
{
    if (! singletonObject)
    {
        singletonObject = [super init];
        [FBSDKAppEvents activateApp];
        [GIDSignIn sharedInstance].delegate = self;
    }
    return singletonObject;
}


//Facebook
//loginToFacebookOnViewController
-(void)loginToFacebookOnViewController:(UIViewController *)vc
                            completion:(void(^)(NSMutableDictionary *info,NSError * error))completion
{
    FBSDKLoginManager *facebookLoginManager = [[FBSDKLoginManager alloc] init];
    [facebookLoginManager logInWithReadPermissions:@[USER_PUBLIC_PROFILE_KEY,USER_EMAIL_KEY]
                                fromViewController:vc
                                           handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
     {
         if(error != nil)
         {
             [facebookLoginManager logOut];
             completion(NULL,error);
         }
         else if([result isCancelled])
         {
             completion(NULL,[self generateErrorForCode:FB_LOGIN_FLOW_CANCELLED_CODE
                                             andMessage:FB_LOGIN_FLOW_CANCELLED_MESSAGE]);
         }
         else
         {
             NSMutableDictionary *info = [[NSMutableDictionary alloc] init];
             [info setObject: result.token.userID forKey:SOCIAL_OAUTHID_KEY];
             [info setObject: result.token.tokenString forKey:SOCIAL_ACCESS_TOKEN_KEY];
             [info setObject:@"facebook" forKey:@"oauthsiteid"];
             
             NSString *picUrl = [NSString stringWithFormat:@"https://graph.facebook.com/v2.7/%@/picture?type=large",result.token.userID];
             
             [info setObject:picUrl forKey:@"picture"];
             
             completion(info,NULL);
             
         }
     }];
    [[FBSDKLoginManager alloc] logOut];
    
}

//Google
-(void)loginToGoogleOnViewController:(id)vc
{
    [GIDSignIn sharedInstance].uiDelegate = vc;
    [GIDSignIn sharedInstance].delegate = self;
    privateGoogleLoginDelegate = vc;
    [[GIDSignIn sharedInstance] signIn];
}
-(void) getGoogleDataForUser:(GIDGoogleUser *)user
{
    
    NSMutableDictionary *info = [[NSMutableDictionary alloc] init];
    [info setObject: user.userID forKey:SOCIAL_OAUTHID_KEY];
    [info setObject: user.authentication.accessToken forKey:SOCIAL_ACCESS_TOKEN_KEY];
    [info setObject:@"googleplus" forKey:@"oauthsiteid"];
    
    // DP url from google plus
    NSURL *url = [user.profile imageURLWithDimension:200];
    [info setObject:[url absoluteString] forKey:@"picture"];
    
    
    [[GIDSignIn sharedInstance] signOut];
    
    [self.privateGoogleLoginDelegate googleSignInSuccessfulWithInfo:info];
}

#pragma mark GIDSignInDelegate
- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error
{
    if(error)
    {
        [self.privateGoogleLoginDelegate googleSignInFailWithError:error];
    }
    else{
        [self getGoogleDataForUser:user];
    }
}

// Passive Login : Get User info from Facebook and login to SSO using the info.
-(void)doPassiveLoginOnCompletion:(void(^)(NSDictionary *info,NSError * err))completion
{
    [self loginToFacebookOnViewController:[[[UIApplication sharedApplication] keyWindow] rootViewController] completion:^(NSMutableDictionary *info, NSError *error)
     {
         if (error!=NULL)
         {
             completion(@{},error);
             return ;
         }
         
         if (deprecatedAllowed)
         {
             [[NSSOCrossAppLoginManager sharedLoginManager] loginUsingSocialInfo:info
                                                                         success:^
              {
                  completion(@{},error);
                  return ;
              }
                                                                         failure:^(NSError * _Nullable error)
              {
                  completion(@{},error);
                  return ;
              }];

         }
         else
         {
                      SSOSocialInfo *si = [[SSOSocialInfo alloc] init];
                      si.accessToken = [info valueForKey:SOCIAL_ACCESS_TOKEN_KEY];
                      si.oauthId = [info valueForKey:SOCIAL_OAUTHID_KEY];
                      [[NSSOCrossAppLoginManager sharedLoginManager] performSocialActivity:SSOFacebookLogin
                                                                           usingSocialInfo:si
                                                                                   success:^{
                                                                                       completion(@{},error);
                                                                                       return ;
                                                                                   }
                                                                                   failure:^(SSOError * _Nullable error)
                       {
                           completion(@{},error);
                           return ;
                       }];
         }
         

     }];
}

-(NSError *)generateErrorForCode:(NSInteger)code andMessage:(NSString *)message{
    NSMutableDictionary* details = [NSMutableDictionary dictionary];
    [details setValue:message  forKey:NSLocalizedDescriptionKey];
    return  [NSError errorWithDomain:ERROR_DOMAIN code:code userInfo:details];
    
}
//Truecaller
-(void)loginToTruecallerOnViewController:(id)vc
{
    [TCTrueSDK sharedManager].delegate = self;
    [[TCTrueSDK sharedManager] requestTrueProfile];
    privateTruecallerLoginDelegate = vc;
}

#pragma mark TCTrueSDKDelegates
- (void)didFailToReceiveTrueProfileWithError:(nonnull TCError *)error
{
    [self.privateTruecallerLoginDelegate truecallerSignInFailWithError:error];
}
- (void)didReceiveTrueProfileResponse:(nonnull TCTrueProfileResponse *)profileResponse
{
    [self getTruecallerDataForResponse:profileResponse];
}
-(void)didReceiveTrueProfile:(TCTrueProfile *)profile
{
    
}
-(void) getTruecallerDataForResponse:(TCTrueProfileResponse *)profileResponse
{
    
    NSMutableDictionary *info = [[NSMutableDictionary alloc] init];
    if (profileResponse != nil)
    {
        [info setObject: profileResponse.payload forKey:TRUE_PAYLOAD_KEY];
        [info setObject: profileResponse.signature forKey:TRUE_SIGNATURE_KEY];
        [info setObject:@"truecaller" forKey:@"oauthsiteid"];
        
        [self.privateTruecallerLoginDelegate truecallerSignInSuccessfulWithInfo:info];
    }
    else
    {
        [self.privateTruecallerLoginDelegate truecallerSignInFailWithError:[self generateErrorForCode:TRUE_PROFILE_NIL_CODE andMessage:TRUE_PROFILE_NIL_MESSAGE]];
    }
}
@end
