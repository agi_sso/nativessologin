//
//  NSObject+NSSOValidator.h
//  NativeSSOLogin
//
//  Created by Pankaj Verma on 19/10/16.
//  Copyright © 2016 Pankaj Verma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSSOValidator)
-(BOOL)isValidEmail;
-(BOOL)isValidMobile;
@end
