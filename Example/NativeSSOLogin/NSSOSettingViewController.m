//
//  NSSOSettingViewController.m
//  NativeSSOLogin
//
//  Created by Pankaj Verma on 09/12/16.
//  Copyright © 2016 Pankaj Verma. All rights reserved.
//

#import "NSSOSettingViewController.h"
#import "NSSOCrossAppLoginManager.h"
#import "NSSOTempGlobal.h"
#import "NSSOTempGlobal.h"
@interface NSSOSettingViewController ()
@property (weak, nonatomic) IBOutlet UITextField *tickedIdField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;

@property (weak, nonatomic) IBOutlet UISwitch *getUserDataEnabled;
@property (weak, nonatomic) IBOutlet UIButton *deprecateButton;

@end

@implementation NSSOSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _deprecateButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    // you probably want to center it
    _deprecateButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapRecognizer];
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)migrateExistingTicket:(id)sender
{
    [[NSSOCrossAppLoginManager sharedLoginManager] migrateCurrentSessionToAppHavingTicketId:_tickedIdField.text
                                                                                 completion:^(NSDictionary *info, NSError *error)
     {
         if (error == NULL)
         {
             //4
             //  oldTicket = [self deleteExistingTicketIdIfAny];
             [self showAlertMessage:@"migrated successfully"];
         }
         else
         {
             [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
             
         }
     }];
}
-(IBAction)close:(id)sender{
    [self dismissViewControllerAnimated:true completion:^{
    }];
}


-(IBAction)copyGlobalSessionToApp:(id)sender
{
    [[NSSOCrossAppLoginManager sharedLoginManager] copySSOGlobalSessionToAppOnCompletion:^(NSDictionary *info, NSError *error) {
        if (error != NULL) {
           [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
        }
        else{
        [self showAlertMessage:@"Global session copied"];
        }
    }];
   
}
- (IBAction)allowDeprecatedAPIs:(id)sender
{
    if (!deprecatedAllowed)
    {
        [_deprecateButton setTitle:@"Currently Deprecated APIs are enabled. Do you want to disable??" forState:UIControlStateNormal];
        [_deprecateButton setNeedsLayout];
        [_deprecateButton layoutIfNeeded];
        deprecatedAllowed = true;
    }
    else
    {
        [_deprecateButton setTitle:@"Currently Deprecated APIs are disabled. Do you want to enable??" forState:UIControlStateNormal];
        [_deprecateButton setNeedsLayout];
        [_deprecateButton layoutIfNeeded];
        deprecatedAllowed = false;

    }
    
}


-(IBAction)getAppSession:(id)sender{
     if (!deprecatedAllowed) {
    [[NSSOCrossAppLoginManager sharedLoginManager] getSSOAppSessionOnCompletion:^(SSOSession * _Nullable session, NSError * _Nullable error)
     {
         if (error != NULL)
         {
             [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
         }
         else{
             [self showAlertMessage:[NSString stringWithFormat:@"ssec = %@\nticketId = %@\ntype = %@\ntgId = %@\nidentifier = %@",session.ssec,session.ticketId,session.type,session.tgId,session.identifier]];
         }
     }];
     }
    else
    {
        [[NSSOCrossAppLoginManager sharedLoginManager] getAppSessionOnCompletion:^(NSDictionary *info, NSError *error)
         {
             if (error != NULL)
             {
                 [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
             }
             else{
                 [self showAlertMessage:[NSString stringWithFormat:@"%@",info]];
             }
         }];
    }
    
}
-(IBAction)getGlobalSession:(id)sender
{
    if (!deprecatedAllowed) {
            [[NSSOCrossAppLoginManager sharedLoginManager] getSSOGlobalSessionWithUserDataEnabled:_getUserDataEnabled.isOn completion:^(SSOSession * _Nullable session, NSError * _Nullable error)
             {
                 if (error != NULL)
                 {
                     [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
                 }
                 else
                 {
                     [self showAlertMessage:[NSString stringWithFormat:@"FirstName:%@\nlastName:%@\ncountryCode:%@\nemail:%@\nmobile:%@\nssec:%@\nticketId:%@\ntgId:%@\ntype:%@\nidentifier:%@\n",session.user.firstName,session.user.lastName,session.user.countryCode,session.user.email,session.user.mobile,session.ssec,session.ticketId,session.tgId,session.type,session.identifier]];
                 }
                 
             }];
    }
    else
    {
        [[NSSOCrossAppLoginManager sharedLoginManager] getGlobalSessionOnCompletion:^(NSDictionary *info, NSError *error) {
            if (error != NULL)
            {
                [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
            }
            else
            {
                [self showAlertMessage:[NSString stringWithFormat:@"%@",info]];
            }
            
        }];
    
    }
}
-(IBAction)renewTicket:(id)sender{
    [[NSSOCrossAppLoginManager sharedLoginManager] renewTicket:^{
        [self showAlertMessage:@"Ticket Refreshed."];
    } failure:^(NSError *error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
}
-(void)showAlertMessage:(NSString *)message{
        [[[UIAlertView alloc] initWithTitle:@"Alert!!" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}

-(IBAction)createAppSession:(id)sender{

[[NSSOCrossAppLoginManager sharedLoginManager] createAppSessionForTicketId:_tickedIdField.text completion:^(NSDictionary * _Nullable info, SSOError * _Nullable error) {
    if (error)
    {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }
    else
    {
        [self showAlertMessage:@"Local Migration successfull."];
    }
}];
}

-(IBAction)deleteAccount:(id)sender{
    
//    [[NSSOCrossAppLoginManager sharedLoginManager] createAppSessionForTicketId:_tickedIdField.text completion:^(NSDictionary * _Nullable info, SSOError * _Nullable error) {
//        if (error)
//        {
//            [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
//        }
//        else
//        {
//            [self showAlertMessage:@"Local Migration successfull."];
//        }
//    }];
//    [[NSSOCrossAppLoginManager sharedLoginManager] deleteUserAccountWithPassword:_passwordField.text onCompletion:^(NSDictionary * _Nullable info, SSOError * _Nullable error) {
//        if (error)
//        {
//            [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
//        }
//        else
//        {
//            [self showAlertMessage:@"Account deleted."];
//        }
//    }];
}
@end
