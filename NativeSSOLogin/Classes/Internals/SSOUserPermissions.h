//
//  SSOUserPermissions.h
//  Bolts
//
//  Created by Pankaj Verma on 23/11/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SSOUserPermissions : NSObject
@property (nonatomic, nullable) NSString * shareDataAllowed;
@property (nonatomic, nullable) NSString * termsAccepted;
@property (nonatomic, nullable) NSString * timespointsPolicy;
- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary;

@end

NS_ASSUME_NONNULL_END
