//
//  SSOUserStatus.h
//  Pods
//
//  Created by Pankaj Verma on 05/04/17.
//
//

#import <Foundation/Foundation.h>

@interface SSOUserStatus : NSObject
//User Status
@property (nonatomic, nullable, readonly) NSString * identifier;
@property (nonatomic, nullable, readonly) NSString * status;
@property (nonatomic, nullable, readonly) NSString * statusCode;
@property (nonatomic, nullable) NSString * shareDataAllowed;
@property (nonatomic, nullable) NSString * termsAccepted;
@property (nonatomic, nullable) NSString * timespointsPolicy;
- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary;
@end
