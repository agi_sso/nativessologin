//
//  NSSOUrlManager.h
//  Pods
//
//  Created by Pankaj Verma on 10/13/16.
//
//

#import <Foundation/Foundation.h>


@interface NSSOUrlRequestManager : NSObject
NS_ASSUME_NONNULL_BEGIN
+(NSMutableURLRequest *)getUrlRequestForBaseUrl:(NSString *)baseUrl
                                          scope:(NSUInteger)scope
                                           path:(NSString *)path
                                     BodyParams:(NSDictionary * _Nullable )params
                                        queries:(NSDictionary * _Nullable )queries;
NS_ASSUME_NONNULL_END
@end
