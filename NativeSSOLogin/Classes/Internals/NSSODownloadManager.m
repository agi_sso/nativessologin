//
//  NSSODownloadManager.m
//  Pods
//
//  Created by Pankaj Verma on 10/13/16.
//
//

#import "NSSODownloadManager.h"
#import "NSSOUrlRequestManager.h"
#import "NSSOGlobal.h"
#import "SSOResponseModel.h"

//Response data
 NSString * const RESPONSE_STATUS_KEY = @"status";
 NSString * const RESPONSE_DATA_KEY = @"data";
 NSString * const RESPONSE_MESSAGE_KEY = @"message";
 NSString * const RESPONSE_MSG_KEY = @"msg";
 NSString * const RESPONSE_SUCCESS = @"SUCCESS";
 NSString * const RESPONSE_FAILURE = @"FAILURE";
 NSString * const RESPONSE_CODE_KEY = @"code";

@implementation NSSODownloadManager
//+ (id)ssoSharedManager
//{
//    static NSSODownloadManager *sharedManager = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken,
//                  ^{
//                      sharedManager = [[self alloc] init];
//                  });
//    return sharedManager;
//}

-(void)downloadDataForUrlImageUpload:(NSString *)baseUrl
                               scope:(NSUInteger)scope
                                path:(NSString *)path
                             andBody:(NSMutableData*)body_data
                   completionHandler:(privateCompletionBlock)completionHandler
{
    
    NSMutableURLRequest * downloadReq = [NSSOUrlRequestManager getUrlRequestForBaseUrl:baseUrl scope:scope path:path BodyParams:NULL queries:NULL];
    [downloadReq setHTTPBody: body_data];
    //override Content-Type
    [downloadReq setValue: [NSString stringWithFormat: @"multipart/form-data; boundary=%@",UUID_BOUNDARY] forHTTPHeaderField: @"Content-Type"];
    
    [self startSessionForRequest:downloadReq scope:scope path:path completionHandler:^(SSOResponseModel * _Nullable model, SSOError * _Nullable error)
     {
         if (!error)
         {
             NSInteger code = [model.code integerValue];
             
             //success
             if (code/100==2)
             {
                 completionHandler(model.data,nil);
             }
             else
             {
                 completionHandler(nil,[SSOError errorWithCode:[model.code integerValue] description:model.message]);
             }
         }
         else
         {
             completionHandler(NULL,error);
         }
     }];
}

-(void)downloadDataForBaseUrl:(NSString *)baseUrl
                        scope:(NSUInteger)scope
                         path:(NSString *)path
                       params:(NSDictionary * _Nullable )params
                      queries:(NSDictionary * _Nullable )queries
            completionHandler:(privateCompletionBlock)completionHandler

{
    NSMutableURLRequest * downloadReq = [NSSOUrlRequestManager getUrlRequestForBaseUrl:baseUrl
                                                                                 scope:scope
                                                                                  path:path
                                                                            BodyParams:params
                                                                               queries:queries];
    
    [self startSessionForRequest:downloadReq scope:scope path:path completionHandler:^(SSOResponseModel * _Nullable model, SSOError * _Nullable error)
     {
         if (!error)
         {
             NSInteger code = [model.code integerValue];
             
             //success
             if (code/100==2)
             {
             completionHandler(model.data,nil);
             }
             else
             {
             completionHandler(nil,[SSOError errorWithCode:[model.code integerValue] description:model.message]);
             }
        }
         else
         {
             completionHandler(NULL,error);
         }
         }];
}

-(void)startSessionForRequest:(NSMutableURLRequest * )downloadRequest
                        scope:(NSUInteger)scope
                         path:(NSString *)path
            completionHandler:(void (^)(SSOResponseModel * _Nullable model,  SSOError * _Nullable error))completionHandler
{
 
    if (![self isSDKInitializedForPath:path])
    {
        completionHandler(NULL,[SSOError errorWithCode:SSOSDKErrorCodeSDKNotInitialized]);
        return;
    }
    
    static NSURLSession *session = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration   defaultSessionConfiguration];
        session = [NSURLSession sessionWithConfiguration:configuration];
    });
    NSURLSessionDataTask *task;
    task = [session dataTaskWithRequest:downloadRequest
                      completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (!error)
                    {
                        SSOResponseModel * model = [self parseObjectFromResponseData:data scope:scope];
                        if (model)
                        {
                            completionHandler(model,nil);
                        }
                        else
                        {
                            completionHandler(model,[SSOError errorWithCode:SSOSDKErrorCodeDataNil]);
                        }
                    }
                    else
                    {
                        completionHandler(nil,[SSOError errorWithCode:error.code description:error.localizedDescription]);
                    }
                });
            }];
    
    [task resume];
}

-(SSOResponseModel *)parseObjectFromResponseData:(NSData *)data
                                           scope:(NSUInteger)scope
{
    NSDictionary *dataDictionary = nil;
    SSOResponseModel *model = nil;
    if ( data != nil && ![data isKindOfClass:[NSNull class]])
    {
         dataDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                       options:0
                                                                         error:nil];
        
    }
    if (dataDictionary)
    {
        model = [[SSOResponseModel alloc] initWithDictionary:dataDictionary];
        if (scope == ssomSocial)
        {
            model.data = dataDictionary;
            model.message = model.msg;
        }
    }
    return model;
}

-(BOOL)isSDKInitializedForPath:(NSString *)path
{
    bool isInitialized = true;
    if (!appHeader.channel||!appHeader.siteId||!appHeader.teamId)
    {
        isInitialized = false;
    }
    
    if (([appSession.tgId length] == 0) && (![path isEqualToString:getDataForDeviceUrlPath]))
    {
        isInitialized = false;
    }
    
    
    return isInitialized;
}

@end
