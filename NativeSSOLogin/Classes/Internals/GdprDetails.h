//
//  GdprDetails.h
//  NativeSSOLogin
//
//  Created by Pankaj Verma on 30/10/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GdprDetails : NSObject
@property (nonatomic, nullable) NSString *email;
@property (nonatomic, nullable) NSString *mobile;
@property (nonatomic, nullable) NSString *password;
@property (nonatomic, nullable) NSString * shareDataAllowed;
@property (nonatomic, nullable) NSString * termsAccepted;
@property (nonatomic, nullable) NSString * timespointsPolicy;
@end

NS_ASSUME_NONNULL_END
